<?php 

namespace Adminsite\Adm\Utils;

interface FileInterface
{
	public function isImage ();
	
	public function isValid ();

	public function getOriginalName ();

	public function getOriginalExtension ();

	public function getMimeType ();

	public function getSize ();

	public function getError ();
}