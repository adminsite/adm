<?php 

namespace Adminsite\Adm\Laravel\Controllers;

use Illuminate\Routing\Controller;
use Adminsite\Adm\Http\Request;
use Intervention\Image\ImageManager;

/**
* 
*/
class ImagesController extends Controller
{
	protected $intervention;
	
	protected $request;

	public function __construct (Request $request, ImageManager $intervention)
	{
		$this->request = $request;
		$this->intervention = $intervention;
	}

	public function index ($modulo, $referencia)
	{
		$param = $this->request->get();
		$ref   = explode('_', $referencia);
		$file  = array_pop($ref); //Archivo
		$path  = implode("/", $ref); //Ruta hacia la imagen

		$w = (isset($param['width'])) ? $param['width'] : 200;
		$h = (isset($param['height'])) ? $param['height'] : null;

		return $this->intervention
					->make("adm/$modulo/$path/$file")
					->fit($w, $h)
					->response('jpg');
	}
}