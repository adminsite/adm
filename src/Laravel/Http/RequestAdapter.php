<?php 

namespace Adminsite\Adm\Laravel\Http;

use Adminsite\Adm\Http\Request as RequestInterface;

/**
* Clase para adaptar y extender la funcionalidad
* de Illuminute\Http\Request en Laravel
* 
*/
class RequestAdapter implements RequestInterface
{
	protected $request;

	protected $files;

	protected $query;

	protected $data = array();

	public function __construct($request)
	{
		$this->parse($request);
	}

	/**
	 * Parsear los datos para adaptar la clase
	 *
	 * @var Illuminate\Http\Request $request
	 * @return void
	 */
	public function parse ($request)
	{
		$this->request = $request;
		$this->query   = $request->query;
		$this->files   = $request->files;

		$this->data = $request->query->all();
		$this->data['offset'] = $this->getOffset();
		$this->data['limit']  = $this->getLimit();
	}

	public function files ()
	{
		return $this->files;
	}

	public function post ()
	{
		return $this->request->all();
	}

	public function get ()
	{
		return $this->query->all();
	}

	public function getOffset ($default = 0)
	{
		return ($this->request->has('offset')) ? (int) $this->query->get('offset') : $default;
	}

	public function getLimit ($default = 10)
	{
		return ($this->request->has('limit')) ? (int) $this->query->get('limit') : $default;
	}

	public function __get ($name)
	{
		if (array_key_exists($name, $this->data)) {
			return $this->data[$name];
		}

		return null;
	}
}