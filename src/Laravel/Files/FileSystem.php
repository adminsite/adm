<?php

namespace Adminsite\Adm\Laravel\Files;

use Adminsite\Adm\Files\FileSystemInterface;
use Illuminate\Filesystem\FileSystem as IlluminateFileSystem;

class FileSystem implements FileSystemInterface
{
	protected $files;

	public function __construct(IlluminateFileSystem $files)
	{
		$this->files = $files;
	}

	public function mkdir ($path, $mode = 777, $recursive = false, $force = false)
	{
		if (is_dir($path)) {
			return true;
		}

		return $this->files->makeDirectory($path, $mode, $recursive, $force);
	}

	public function rmdir ($path)
	{
		return $this->files->deleteDirectory($path, true);
	}
}