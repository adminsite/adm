<?php 

namespace Adminsite\Adm\Upload;

class File extends FileInterface
{
	protected $file;
	
	public function __construct($file)
	{
		$this->file = $file;
	}

	public function isImage ()
	{
		return ( strpos(strtolower($this->getMimeType()), 'image') === 0 );
	}

	public function isValid ()
	{
		return $this->file->isValid();
	}

	public function getOriginalName ()
	{
		return $this->file->getClientOriginalName();
	}

	public function getOriginalExtension ()
	{
		return $this->file->getClientOriginalExtension();
	}

	public function getMimeType ()
	{
		return $this->file->getClientMimeType();
	}

	public function getSize ()
	{
		return $this->file->getClientSize();
	}

	public function getError ()
	{
		return $this->file->getError();
	}
}