<?php 

namespace Adminsite\Adm\Storage;

interface RepositoryInterface
{
	public function all ();

	public function read ($offset, $limit);

	public function find ($id);

	public function create ($request);

	public function update ($request, $id);

	public function delete ($id);

	public function getModel ();
} 