<?php 

namespace Adminsite\Adm\Files;

interface FileSystemInterface
{
	public function mkdir ($path, $mode = 777, $recursive = false, $force = false);

	public function rmdir ($path);
}