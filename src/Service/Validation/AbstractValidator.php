<?php 

namespace Adminsite\Adm\Service\Validation;

abstract class AbstractValidator
{
	/**
	 * Validador
	 *
	 * @var object
	 */
	protected $validator;
	 
	/**
	 * Datos que se tienen que validar
	 *
	 * @var array
	 */
	protected $data = array();
	 
	/**
	 * Reglas de Validacion
	 *
	 * @var array
	 */
	protected $rules = array();
	 
	/**
	 * Errores de Validacion
	 *
	 * @var array
	 */
	protected $errors = array();

	/**
	 * Set data to validate
	 *
	 * @param array $data
	 * @return self
	 */
	public function with(array $data)
	{
		$this->data = $data;

		return $this;
	}

	/**
	 * Pass the data and the rules to the validator
	 *
	 * @return boolean
	 */
	abstract function passes();

	/**
	 * Return errors
	 *
	 * @return array
	 */
	public function errors()
	{
		return $this->errors;
	}
}