<?php 

namespace Adminsite\Adm\Service\Validation;

interface ValidableInterface 
{
	/**
	 * With
	 *
	 * @param array
	 * @return object
	 */
	public function with (array $input);

	/**
	 * Passes
	 *
	 * @return boolean
	 */
	public function passes ();

	/**
	 * Errors
	 *
	 * @return array
	 */
	public function errors ();
}