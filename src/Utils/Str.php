<?php 

namespace Adminsite\Adm\Utils;

class Str
{
	public static function slug ($text, $separator = '-')
	{
		$text = htmlentities($text, ENT_QUOTES, 'UTF-8');
		$text = strtolower($text);
		$patron = array (
			// Espacios, puntos y comas por guion
			//'/[\., ]+/' => ' ',
 
			// Vocales
			'/\+/'       => '',
			'/&agrave;/' => 'a',
			'/&egrave;/' => 'e',
			'/&igrave;/' => 'i',
			'/&ograve;/' => 'o',
			'/&ugrave;/' => 'u',
			
			'/&aacute;/' => 'a',
			'/&eacute;/' => 'e',
			'/&iacute;/' => 'i',
			'/&oacute;/' => 'o',
			'/&uacute;/' => 'u',
			
			'/&acirc;/'  => 'a',
			'/&ecirc;/'  => 'e',
			'/&icirc;/'  => 'i',
			'/&ocirc;/'  => 'o',
			'/&ucirc;/'  => 'u',
			
			'/&atilde;/' => 'a',
			'/&etilde;/' => 'e',
			'/&itilde;/' => 'i',
			'/&otilde;/' => 'o',
			'/&utilde;/' => 'u',
			
			'/&auml;/'   => 'a',
			'/&euml;/'   => 'e',
			'/&iuml;/'   => 'i',
			'/&ouml;/'   => 'o',
			'/&uuml;/'   => 'u',
			
			'/&auml;/'   => 'a',
			'/&euml;/'   => 'e',
			'/&iuml;/'   => 'i',
			'/&ouml;/'   => 'o',
			'/&uuml;/'   => 'u',
 
			// Otras letras y caracteres especiales
			'/&aring;/' => 'a',
			'/&ntilde;/' => 'n',
 
			// Agregar aqui mas caracteres si es necesario
 
		);
 
		$text = preg_replace(array_keys($patron),array_values($patron),$text);

		// Convert all dashes/underscores into separator
		$flip = $separator == '-' ? '_' : '-';

		$text = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $text);

		$text = preg_replace('!['.preg_quote($flip).']+!u', $separator, $text);

		// Remove all characters that are not the separator, letters, numbers, or whitespace.
		$text = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($text));

		// Replace all separator characters and whitespace by a single separator
		$text = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $text);

		return trim($text, $separator);
	}
}