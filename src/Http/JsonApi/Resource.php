<?php 

namespace Adminsite\Adm\Http\JsonApi;

/**
* 
*/
class Resource implements ResourceInterface
{
	private $type;

	private $id         = null;
	
	private $attributes = array();
	
	private $links      = array();
	
	private $relations  = array();

	public function __construct(string $type, $data = array())
	{
		$this->setAttributes($data);
	}

	public function addRelation (string $name, ResourceInterface $relation)
	{
		$rel = array(
			"data" => array(
				"type" => $relation->getType(),
				"id"   => $relation->getId()
			)
		);

		$l_self    = $resource->getSelfLink();
		$l_related = $resource->getRelatedLink();
		$links     = array();

		if ($this->validateUrl($l_self)) {
			$links["self"] = $l_self;
			$rel['links'] = $links;
		}

		if ($this->validateUrl($l_related)) {
			$links["related"] = $l_related;
			$rel['links'] = $links;
		}

		$this->relations[$name] = $rel;
	}

	/**
	 * Filtra el array para corroborar los valores correctos
	 *
	 * @param mixed $val
	 * @return bool
	 */
	private function filterAttributes ($val)
	{
		return (is_numeric($val) or is_string($val) or is_bool($val));
	}

	public function getId ()
	{
		return $this->id;
	}

	public function getType ()
	{
		return $this->type;
	}

	public function getAttributes ()
	{
		return $this->attributes;
	}

	public function getSelfLink ()
	{
		if (array_key_exists('self', $this->links)) {
			return $this->links['self'];
		}
	}
	
	public function getRelatedLink ()
	{
		if (array_key_exists('related', $this->links)) {
			return $this->links['related'];
		}
	}

	/**
	 * Establece valores el los atributos
	 *
	 * @param array $attr
	 * @return self
	 */
	public function setAttributes (array $attr)
	{
		if (array_key_exists("id", $attr)) {
			$this->id = $attr['id'];
			unset($attr['id']);
		}

		$attributes = array_filter($attr, array($this, 'filterAttributes'));

		$this->attributes = array_merge($this->attributes, $attributes);

		return $this;
	}

	/**
	 * 
	 */
	public function setSelfLink ($url)
	{
		if ($this->validateUrl($url)) {
			$this->links['self'] = $url;
		}
	}

	/**
	 * 
	 */
	public function setRelatedLink ($url)
	{
		if ($this->validateUrl($url)) {
			$this->links['related'] = $url;
		}
	}

	/**
	 * 
	 */
	public function setType ($type)
	{
		$this->type = $type;
	}

	/**
	 * 
	 */
	public function toArray ()
	{
		$arr = array(
			"type"       => $this->type,
			"attributes" => (array) $this->attributes
		);

		//Si existe el ID
		if (!is_null($this->id)) {
			$arr['id'] = $this->id;
		}

		if (!empty($this->links)) {
			$arr['links'] = $this->links;
		}

		if (!empty($this->relations)) {
			$arr['relations'] = $this->relations;
		}

		return $arr;
	}

	/**
	 * Validate that an attribute is a valid URL.
	 *
	 * @param  string  $value
	 * @return bool
	 */
	protected function validateUrl($value)
	{
		return filter_var($value, FILTER_VALIDATE_URL) !== false;
	}

	public function __set ($name, $val)
	{
		if ($name == "id") {
			$this->id = $val;
		} else if ($this->filterAttributes($val)) {
			$this->attributes[$name] = $val;
		}
	}

	public function __get ($name)
	{
		if (array_key_exists($name, $this->attributes)) {
			return $this->attibutes[$name];
		}
	}
}