<?php 

namespace Adminsite\Adm\Http\JsonApi;

use Adminsite\Adm\Http\JsonApi\Resource;

/**
* 
*/
class Collection
{
	private $type;

	private $resources = array();

	public function setType ($type)
	{
		$this->type = $type;
	}

	public function add ($resource)
	{
		$this->resources[] = $resource;
	}

	public function toArray ()
	{
		$count = count($this->resources);

		
		if ($count == 1) {
			return array_shift($this->resources);
		}

		return $this->resources;
	}
}