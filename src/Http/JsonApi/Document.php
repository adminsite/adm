<?php 

namespace Adminsite\Adm\Http\JsonApi;

use Adminsite\Adm\Http\JsonApi\Resource;
use Adminsite\Adm\Http\JsonApi\Collection;
use Adminsite\Adm\Http\JsonApi\ResourceBuilder;

/**
* 
*/
class Document
{
	private $type;

	private $data;
	
	private $errors;

	private $builder;
	
	private $relations = array();

	public function __construct(Collection $collection, ResourceBuilder $builder)
	{
		$this->errors  = null;
		$this->data    = $collection;
		$this->builder = $builder;
	}

	public function setType ($type)
	{
		if (is_string($type) and strlen($type) > 0) {
			$this->type = $type;
			$this->data->setType($type);
		}
	}

	public function add (ResourceInterface $resource)
	{
		$this->data->add( $this->builder->make($resource) );
		return $this;
	}

	public function addCollection (array $resources)
	{
		$c = count($resources);
		for ($i=0; $i < $c; $i++) { 
			$this->add($resources[$i]);
		}

		return $this;
	}

	/**
	 * En construccion
	 */
	public function error ($error)
	{
		$this->errors = $error;
	}

	public function response ()
	{
		return $this->toArray();
	}

	public function toArray ()
	{
		$document = array();

		if (!is_null($this->errors)) {
			$document['errors'] = $this->errors;
		} else {
			$document['data'] = $this->data->toArray();
		}

		return $document;
	}
}