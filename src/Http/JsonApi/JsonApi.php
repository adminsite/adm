<?php 

namespace Adminsite\Adm\Http\JsonApi;

use Adminsite\Adm\Http\JsonApi\Document;
use Adminsite\Adm\Http\JsonApi\Collection;
use Adminsite\Adm\Http\JsonApi\Resource;

/**
* 
*/
class JsonApi
{
	public function makeDocument ($type, array $data=null)
	{
		return self::make($type, $data);
	}
	
	static public function make ($type, $data=null)
	{
		$obj = new Document( new Collection, new ResourceBuilder );
		$obj->setType($type);

		if (!is_null($data)) {
			$obj->add($data);
		}

		return $obj;
	}

	static public function resource (array $data)
	{
		return new Resource($data);
	}
}