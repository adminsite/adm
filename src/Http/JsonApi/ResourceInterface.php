<?php 

namespace Adminsite\Adm\Http\JsonApi;

interface ResourceInterface
{
	public function getId ();

	public function getType ();

	public function getAttributes ();

	public function getSelfLink ();
	
	public function getRelatedLink ();

	public function getRelationships ();
}