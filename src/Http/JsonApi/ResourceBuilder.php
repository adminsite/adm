<?php 

namespace Adminsite\Adm\Http\JsonApi;

class ResourceBuilder
{
	public function make ($data, array $omit=array())
	{
		if ($data instanceof ResourceInterface) {
			return $this->buildFromInterface($data, $omit);
		}/* elseif (is_array($data)) {
			$this->buildFromArray($data, $omit);
		}*/
	}

	private function buildFromInterface ($resource, $omit)
	{
		$arr = array(
			"type" => $resource->getType(),
			"id"   => $resource->getId()
		);

		//Atributos
		if (!in_array("attributes", $omit)) {
			$attributes = $resource->getAttributes();

			if (!is_null($attributes)) {
				$arr['attributes'] = $attributes;				
				unset($arr['attributes']['id']);
				unset($arr['attributes']['type']);
			}
		}

		if (!in_array("relationships", $omit)) {
			$relationships = $resource->getRelationships();
			
			if (!is_null($relationships)) {
				$arr['relationships'] = $relationships;
			}
		}

		//Links
		/*if (!in_array("links", $omit)) {
			$l_self    = $resource->getSelfLink();
			$l_related = $resource->getRelatedLink();
			$links     = array();

			if ($this->validateUrl($l_self)) {
				$links["self"] = $l_self;
				$arr['links'] = $links;
			}

			if ($this->validateUrl($l_related)) {
				$links["related"] = $l_related;
				$arr['links'] = $links;
			}
		}*/

		return $arr;
	}

	/*private function buildFromArray ($resource, $omit)
	{
		if (array_key_exists("id", $attr)) {
			$this->id = $attr['id'];
			unset($attr['id']);
		}

		$attributes = array_filter($attr, array($this, 'filterAttributes'));

		$this->attributes = array_merge($this->attributes, $attributes);

		return $this;
	}*/
}