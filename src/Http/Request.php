<?php 

namespace Adminsite\Adm\Http;

interface Request
{
	public function files ();

	public function post ();

	public function get ();

	public function getOffset ($default);

	public function getLimit ($default);
}