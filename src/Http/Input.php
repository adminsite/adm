<?php 

namespace Adminsite\Adm\Http;

/**
* 
*/
class Input implements InputInterface
{
	private $data;

	public function set (array $data)
	{
		$this->data = $data;
		return $this;
	}

	public function has ($name)
	{
		return array_key_exists($name, $this->data);
	}

	public function __set ($name, $val)
	{
		$this->data[$name] = $val;
	}

	public function __get ($name)
	{
		if ($this->has($name)) {
			return $this->data[$name];
		}

		return "";
	}
}