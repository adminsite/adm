<?php 

namespace Adminsite\Adm\Http;

interface InputInterface
{
	public function set (array $data);

	public function has ($name);
}