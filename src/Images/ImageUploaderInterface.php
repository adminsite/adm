<?php 

namespace Adminsite\Adm\Images;

interface ImageUploaderInterface 
{
	public function get ($name);

	public function has ($name);
}