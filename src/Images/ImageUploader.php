<?php 

namespace Adminsite\Adm\Images;

use Adminsite\Adm\Http\Request;
use Adminsite\Adm\Images\Imagen;

class ImageUploader implements ImageUploaderInterface
{
	/**
	 * Array de archivos recibidos
	 * @var array
	 */
	protected $collection = array();

	protected $validators;

	public function __construct(Request $request, $validators = array())
	{
		$this->validators = $validators;

		$this->collection = $this->parseFiles($request->files()->all());
	}

	/**
	 * Parsear imagenes
	 *
	 * @param $request
	 * @return void
	 */
	protected function parseFiles ($files, $key = null)
	{
		$arr = array();

		foreach ($files as $key => $item) 
		{
			if (is_array($item)) 
			{
				$arr[$key] = $this->parseFiles($item, $key);
			}
			else
			{
				$imagen = new Imagen;
				$arr[$key] = $imagen->make($item);
			}
		}

		return $arr;
	}

	public function get ($name)
	{
		if ($this->has($name, $this->collection)) {
			return $this->collection[$name];
		}

		return null;
	}

	public function has ($name)
	{
		return array_key_exists($name, $this->collection);
	}
}