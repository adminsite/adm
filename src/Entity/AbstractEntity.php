<?php 

namespace Adminsite\Adm\Entity;

use Exception;
 
abstract class AbstractEntity {

	/**
	 * All
	 *
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function all()
	{
		return $this->repository->all();
	}

	/**
	 * Find
	 *
	 * @return Illuminate\Database\Eloquent\Model
	 */
	public function find($id)
	{
		return $this->repository->find($id);
	}

	/**
	 * Read
	 *
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function read ($offset, $limit)
	{
		return $this->repository->read($offset, $limit);
	}

	/**
	 * Create
	 *
	 * @param array $data
	 * @return boolean
	 */
	public function create(array $data)
	{
		if( ! $this->createValidator->with($data)->passes() )
		{
			$this->errors = $this->createValidator->errors();
			throw new Exception("Error Processing Request", 1);
		}

		return $this->repository->create( $this->input->set($data) );
	}

	/**
	 * Update
	 *
	 * @param array $data
	 * @return boolean
	 */
	public function update(array $data, $id)
	{
		if( ! $this->updateValidator->with($data)->passes() )
		{
			$this->errors = $this->updateValidator->errors();
			throw new Exception("Error Processing Request", 1);
		}

		return $this->repository->update($this->input->set($data), $id);
	}

	/**
	 * Delete
	 *
	 * @return boolean
	 */
	public function delete($id)
	{
		return $this->repository->delete($id);
	}

	/**
	 * Errors
	 *
	 * @return Illuminate\Support\MessageBag
	 */
	public function errors()
	{
		return $this->errors;
	}

	/**
	 * Devuelve el modelo actual
	 * 
	 * @return object
	 */
	public function getModel ()
	{
		return $this->repository->getModel();
	}
}