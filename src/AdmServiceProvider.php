<?php 
namespace Adminsite\Adm;

use Illuminate\Support\ServiceProvider;
use Adminsite\Adm\Laravel\Eloquent\Model;
use Adminsite\Adm\Images\ImageUploader;
use Adminsite\Adm\Laravel\Http\RequestAdapter;
use Adminsite\Adm\Http\Input;
use Adminsite\Adm\Laravel\Files\FileSystem;

class AdmServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('Adminsite\Adm\Http\Request', function($app){
			return new RequestAdapter($app['request']);
		});

		$this->app->bind('Adminsite\Adm\Http\InputInterface', function($app){
			return new Input();
		});

		//Alias de Intervention\Image\ImageManager
		$this->app->bind('Adminsite\Adm\Images\ImageUploaderInterface', function($app){
			return new ImageUploader($app->make('Adminsite\Adm\Http\Request'));
		});

		$this->app->bind('Adminsite\Adm\Files\FileSystemInterface', function($app){
			return new FileSystem($app->make('files'));
		});

		$this->app->bind('Adminsite\Adm\Http\RequestHelper', function($app) {
			return new RequestHelper( $app['request'] );
		});

		/**
		 * Rutas del Package
		 *
		 */
		$routes = $this->app['router'];
		$routes->group([
			'prefix'    => 'adm/imagenes/v1/',
			'namespace' => 'Adminsite\Adm\Laravel\Controllers'
		], function () use ($routes) {
			$routes->get('{modulo}/{referencia}', 'ImagesController@index');
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
