<?php 

namespace Adminsite\Adm\Test\Http\JsonApi;

use Mockery;
use Adminsite\Adm\Http\JsonApi\JsonApi;
use Adminsite\Adm\Http\JsonApi\Document;
use Adminsite\Adm\Http\JsonApi\Collection;
use Adminsite\Adm\Http\JsonApi\Resource;

class DocumentTest extends \PHPUnit_Framework_TestCase
{
	public function testJsonApiFactory ()
	{
		$obj = JsonApi::make('prueba');
		$this->assertInstanceOf('Adminsite\Adm\Http\JsonApi\Document', $obj);
	}

	/**
	 * @dataProvider attributesProvider
	 */
	public function testCorrectDocumentStructureOneItem ($params)
	{
		$doc = new Document( new Collection );
		$doc->setType("personas");
		$doc->add( $params );
		
		$arr = array(
			"data" => array(
				"type" => "personas",
				"attributes" => $params
			)
		);
		$this->assertEquals($arr, $doc->toArray());
	}

	public function testErrorDocumentStructure ()
	{
		$obj = JsonApi::make('prueba');

		$obj->error(1);
		$document = $obj->toArray();

		$this->assertArrayHasKey('errors', $document);
		$this->assertFalse(array_key_exists('data', $document));
	}

	public function attributesProvider ()
	{
		return array(
			array( 
				array('nombre'=>'David', 'trabajo'=>'Programador') 
			)
		);
	}
}