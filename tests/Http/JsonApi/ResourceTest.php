<?php 

namespace Adminsite\Adm\Test\Http\JsonApi;

use Mockery;
use Adminsite\Adm\Http\JsonApi\Resource;

class ResourceTest extends \PHPUnit_Framework_TestCase
{	
	public function testSobrecarga ()
	{
		$res = new Resource;
		$res->setType("personas");
		$res->nombre = "David";

		$this->assertEquals(array("type"=>"personas", "attributes"=>array("nombre"=>"David")), $res->toArray());
	}

	public function testResourceId ()
	{
		$res = new Resource(array("id"=>2, "nombre"=>"David"));
		$res->setType("personas");

		$this->assertEquals(array("type"=>"personas", "id"=>2, "attributes"=>array("nombre"=>"David")), $res->toArray());
	}

	public function testFilterAttributes ()
	{
		$res = new Resource(array("otro"=>new \stdClass, "nombre"=>"David"));
		$res->setType("personas");
		
		$this->assertEquals(array("type"=>"personas", "attributes"=>array("nombre"=>"David")), $res->toArray());
	}
}