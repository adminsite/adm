<?php 

namespace Adminsite\Adm\Test\Http\JsonApi;

use Mockery;
use Adminsite\Adm\Http\JsonApi\JsonApi;
use Adminsite\Adm\Http\JsonApi\Document;
use Adminsite\Adm\Http\JsonApi\Collection;
use Adminsite\Adm\Http\JsonApi\Resource;

class CollectionTest extends \PHPUnit_Framework_TestCase
{	
	public function testSetAttributesOnCollection ()
	{
		$mock = $this->getMock('Adminsite\Adm\Http\JsonApi\Collection');
		$mock->expects($this->once())->method('addResource');

		$doc = new Document($mock);
		$doc->setType("articulos");
		$doc->add( array() );
	}

	public function testSetTypeResoruce ()
	{
		$mock = $this->getMock('Adminsite\Adm\Http\JsonApi\Resource');
		$mock->expects($this->once())->method('setType');

		$collection = new Collection;
		$collection->setType("articulos");
		$collection->addResource( $mock );
	}

	public function testSameTypeCollectionResource ()
	{
		$res = new Resource();

		$collection = new Collection;
		$collection->setType("articulos");
		$collection->addResource( $res );

		$this->assertEquals(["type"=>"articulos", "attributes"=>[]], $collection->toArray());
	}
}